#! /usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask
from flask import request, jsonify, render_template
app = Flask(__name__)
from flask_cors import CORS
import program
import HumanAttributes
import time
cors = CORS(app)

def get_success_result():
    return jsonify({"result": "success"})

def get_error_result(error_mess):
    return jsonify({ "result": "failure",   "error": error_mess })

@app.route('/', methods=['GET'])
def hello():
    return 'hello this is attribute selector service :D '

@app.route('/attribute_selector', methods=['POST', 'GET'])
def get_query_sentence():
    #start = time.time()
    try:
        sentence = request.values.get('sentence')
        relations = program.call_get_relations(sentence)
        results = {"result": "success"}
        results['answers'] = relations
        #end = time.time()
        #results['time'] = end - start
        return jsonify(results)
    except Exception as e:
        return get_error_result(str(e))

if __name__ == '__main__':
    HumanAttributes.init()
    app.run(host = '0.0.0.0', port = 7100)
