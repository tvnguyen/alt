import numpy as np
from spacy.en import English


def init():
    global nlp, pronouns_s, pronouns_only_s, pronouns_p, pronouns_o, pronouns_map, ordp, tokens, NPs, NPs_only

    nlp = English()

    pronouns_s = ['i', 'you', 'he', 'she', 'it', 'we', 'they']
    pronouns_only_s = ['i', 'he', 'she', 'we', 'they']
    pronouns_p = ['my', 'your', 'his', 'her', 'its', 'our', 'their']
    pronouns_o = ['mine', 'yours', 'his', 'hers', 'its', 'ours', 'theirs']
    dets = ['a', 'an', 'the']
    NPs_only = ['NP', 'NNP']
    orgs = ['NORP', 'FACILITY', 'ORG', 'GPE', 'EVENT']
    pronouns_map = {}

    ordp = {}
    ordp['i'] = ordp['my'] = ordp['mine'] = 1
    ordp['you'] = ordp['your'] = ordp['yours'] = 2
    ordp['he'] = ordp['his'] = ordp['his'] = 3
    ordp['she'] = ordp['her'] = ordp['hers'] = 4
    ordp['it'] = ordp['its'] = ordp['its'] = 5
    ordp['we'] = ordp['our'] = ordp['ours'] = 6
    ordp['they'] = ordp['their'] = ordp['theirs'] = 7
    
    pronouns_map['my'] = 'I'
    pronouns_map['your'] = 'You'
    pronouns_map['his'] = 'He'
    pronouns_map['her'] = 'She'
    pronouns_map['its'] = 'It'
    pronouns_map['our'] = 'We'
    pronouns_map['their'] = 'They'
    
    tokens = []
    NPs = []

def clearAll():
    global tokens, NPs
    
    del tokens[:]
    del NPs[:]

def clearSentence():
    global tokens, NPs
    
    del tokens[:]
    del NPs[:]

def clean(sent):
    while '  ' in sent:
        sent = sent.replace('  ', ' ')
    return sent

def countStops(startW, endW):
    n = 0
    for i in range(startW, endW):
        if (tokens[i]['is_stop']):
            n += 1
    return n

def tokenSt(startW, endW):
    st = ''
    for i in range(startW, endW):
        st += ' ' + tokens[i]['token']
    st = st.strip()
    return st

def postagSt(startW, endW):
    st = ''
    for i in range(startW, endW):
        st += ' ' + tokens[i]['pos']
    st = st.strip()
    return st

def lemmaSt(startW, endW):
    st = ''
    for i in range(startW, endW):
        st += ' ' + tokens[i]['lemma']
    st = st.strip()
    return st

def stemVB(startW, endW):
    if (startW >= endW):
        return ('', '')

    st = ''
    for i in range(startW, min(startW+3, endW)):
        if (tokens[i]['pos'] == 'VERB') or (tokens[i]['token'] == 'not') or (tokens[i]['token'] == 'n\'t'):
            st += ' ' + tokens[i]['token']
    st = st.strip()

    stem = ''
    verb3rd = ''
    i = startW
    #print(st)
    if (st.startswith('has been')) or (st.startswith('have been'))             or (st.startswith('\'s been')) or (st.startswith('\'ve been')):
        stem = 'be'
        verb3rd = 'is'
        i += 2
    elif (st.startswith('has not been')) or (st.startswith('have not been'))             or (st.startswith('hasn\'t been')) or (st.startswith('haven\'t been')):
        stem = 'not be'
        verb3rd = 'is not'
        if (tokens[i] == 'hasn\'t') or (tokens[i] == 'haven\'t'):
            i += 2
        else:
            i += 3
    while (i < endW):
        if (tokens[i]['pos'] == 'VERB'):
            if (tokens[i]['token'].endswith('ing')):
                stem += ' ' + tokens[i]['token']
                verb3rd += ' ' + tokens[i]['token']
            elif (tokens[i]['lemma'] == 'be'):
                stem += ' ' + tokens[i]['lemma']
                verb3rd += ' is'
            else:
                stem += ' ' + tokens[i]['lemma']
                if (i == startW):
                    if (tokens[i]['token'].startswith(tokens[i]['lemma'])) and (not tokens[i]['token'].endswith('ed')):
                        verb3rd += ' ' + convertVerb3rd(tokens[i]['token'])
                    else:
                        verb3rd += ' ' + tokens[i]['token']
                else:
                    verb3rd += ' ' + tokens[i]['lemma']
        i += 1
    if (tokens[endW-1]['pos'] == 'ADP'):
        stem += ' ' + tokens[endW-1]['token']
        verb3rd += ' ' + tokens[endW-1]['token']

    stem = stem.strip()
    verb3rd = verb3rd.strip()

    return (stem, verb3rd)

def overlapping(NP1, NP2):
    startW1 = NP1['startW']
    endW1 = NP1['endW']
    startW2 = NP2['startW']
    endW2 = NP2['endW']
    
    if (startW1 <= startW2) and (endW2 <= endW1):
        return True
    if (startW2 <= startW1) and (endW1 <= endW2):
        return True

    return False

def expandNP_1(startW, endW):
    i = endW-1
    while (i >= startW) and (tokens[i]['pos'] == 'NOUN'):
        i -= 1
    if (i == startW-1):
        while (i >= 0) and (tokens[i]['pos'].startswith('AD')) and (tokens[i]['token'] not in pronouns_p):
            i -= 1
        if (i >= 0) and (tokens[i]['token'] in pronouns_p):
            newStart = i
            return newStart
    return startW

def expandNP_2():
    global NPs
    n = len(tokens)
    m = len(NPs)
    i = 0
    while (i < m):
        NP = NPs[i]
        startW = NP['startW']
        endW = NP['endW']
        start = NP['start']
        end = NP['end']
        j = startW-1
        if (tokens[j]['pos'] == 'NUM'):
            while (j >= 0) and ( (tokens[j]['pos'] == 'NUM') or (tokens[j]['pos'] == 'PUNCT')                 or (tokens[j]['pos'] == 'SYM') ):
                j -= 1
            if (j >= 0):
                j += 1
            else:
                j = 0
            if (startW != j) and (tokens[j]['pos'] == 'NUM'):
                startW = j
                NP['startW'] = startW
                start = tokens[startW]['start']
                NP['start'] = start
                NP['span'] = sent[start:end]
                NP['pos'] = postagSt(startW, endW)
                NP['lemma'] = lemmaSt(startW, endW)
                for j in reversed(range(i)):
                    if (overlapping(NP, NPs[j])):
                        del NPs[j]
                        i -= 1
                        m -= 1
                        break

                continue

        i += 1

def insert2NPs(span, start, end, startW, endW, pos, lemma, head, NE):
    global NPs

    m = len(NPs)
    if (m == 0):
        newNP = dict(span=span, start=start, end=end, startW=startW, endW=endW, pos=pos, lemma=lemma, head=head, NE=NE)
        NPs.append(newNP)
    else:
        i = 0
        while (i < m) and (NPs[i]['start'] < start):
            i += 1
        if (i < m):
            NP = NPs[i]
        else:
            i -= 1
            NP = NPs[i]

        if (start == NP['start']) and (end == NP['end']):
            if (NP['NE'] in NPs_only) and (NE not in NPs_only):
                NP['NE'] = NE
        else:
            if (end <= NP['start']) or (NP['end'] <= start):
                newNP = dict(span=span, start=start, end=end, startW=startW, endW=endW, pos=pos, lemma=lemma, head=head, NE=NE)
                NPs.insert(i+1, newNP)
            if ((start <= NP['start']) and (NP['end'] <= end)):
                del NPs[i]
                newNP = dict(span=span, start=start, end=end, startW=startW, endW=endW, pos=pos, lemma=lemma, head=head, NE=NE)
                NPs.insert(i, newNP)
            elif ((start <= NP['start']) and (NP['start'] <= end) and (end <= NP['end']) ) or ((NP['start'] <= start) and (start <= NP['end']) and (NP['end'] <= end) ):
                if (NP['end']-NP['start'] < end - start):
                    del NPs[i]
                    newNP = dict(span=span, start=start, end=end, startW=startW, endW=endW, pos=pos, lemma=lemma, head=head, NE=NE)
                    NPs.insert(i, newNP)

def pre_process(sentence):
    global sent, tokens, NPs
    sent = clean(sentence.strip())
    sent = unicode(sent, 'utf-8')
    tks = nlp(sent)
    for tk in tks:
        token = tk.text.strip()
        start = tk.idx
        end = start + len(token)
        is_stop = tk.is_stop
        pos = tk.pos_
        lemma = tk.lemma_
        tokens.append(dict(token_Ori=token, token=token.lower(), start=start, end=end, is_stop=is_stop, pos=pos, lemma=lemma))

    for NP in tks.noun_chunks:
        span = NP.text.strip()
        startW = NP.start
        endW = NP.end
        startW = expandNP_1(startW, endW)
        start = tokens[startW]['start']
        end = tokens[endW-1]['end']
        span = sent[start:end]
        pos = postagSt(startW, endW)
        lemma = lemmaSt(startW, endW)
        head = NP.root.text.lower()
        head_pos = tks[NP.root.i].pos_
        NE = 'NP'
        if (head_pos == 'PROPN'):
            NE = 'NNP'
        insert2NPs(span, start, end, startW, endW, pos, lemma, head, NE)

    for NP in tks.ents:
        span = NP.text.strip()
        startW = NP.start
        endW = NP.end
        startW = expandNP_1(startW, endW)
        start = tokens[startW]['start']
        end = tokens[endW-1]['end']
        span = sent[start:end]
        pos = postagSt(startW, endW)
        lemma = lemmaSt(startW, endW)
        head = NP.root.text.lower()
        head_pos = tks[NP.root.i].pos_
        NE = NP.label_
        insert2NPs(span, start, end, startW, endW, pos, lemma, head, NE)

    expandNP_2()

def check_pattern_1(entity1, entity2):
    relation = ''
    start1 = entity1['startW']
    end1 = entity1['endW']
    start2 = entity2['startW']
    end2 = entity2['endW']
    if (tokens[start1]['token'] in pronouns_p):
        pro = tokens[start1]['token']
        i = start1 + 1
        while (i < end1) and ( tokens[i]['pos'].startswith('AD') ):
            i += 1
        start = i
        end = end1
        relation = lemmaSt(start, end)
        if (pronouns_map[pro].lower() != entity2['head']):
            #return (relation.strip(), pronouns_map[pro], entity2['span'])
            return relation.strip()
    elif (tokens[start2]['token'] in pronouns_p):
        pro = tokens[start2]['token']
        i = start2 + 1
        while (i < end2) and ( tokens[i]['pos'].startswith('AD') ):
            i += 1
        start = i
        end = end2
        relation = lemmaSt(start, end)
        if (pronouns_map[pro].lower() != entity1['head']):
            #return (relation.strip(), entity1['span'], pronouns_map[pro])
            return relation.strip()
    return ''

def check_pattern_2():
    relation = {'stem':'', 'verb3rd':'', 'mid':''}
    n = len(tokens)
    j = -1
    #print(i)
    i = 0
    startW = endW = i
    while (i < n) and (tokens[i]['pos'] != 'VERB'):
        i += 1
    if (i < n):
        j = i
        while (j < n) and (tokens[j]['pos'] == 'VERB'):
            j += 1
        startW = i
        endW = j
        #print('1', startW, endW)
    if (i+1 == j):
        while (j < n) and (j-i <= 2) and (tokens[j]['pos'] == 'ADV'):
            j += 1
        if (j < n) and (tokens[j]['pos'] == 'VERB'):
            while (j < n) and (tokens[j]['pos'] == 'VERB'):
                j += 1
            endW = j
        #print('2', startW, endW)
    if (i < endW) and (endW < n):
        if (tokens[endW]['pos'] == 'ADP'):
            endW += 1
    
    #print('3', startW, endW)
    if (endW <= n):
        (stem, verb3rd) = stemVB(startW, endW)
        relation['stem'] = stem
        relation['verb3rd'] = verb3rd
        relation['mid'] = tokenSt(endW, n).replace(' my ', ' your ')

    return relation['stem']

def generate_Patterns():
    global NPs
    relations = []
    n = len(tokens)
    m = len(NPs)
    for i in range(m):
        tok1 = NPs[i]['span'].lower()
        inNE = False
        j = i+1
        while (j < m) and (not inNE):
            tok2 = NPs[j]['span'].lower()
            if (tok1 not in ordp) or (tok2 not in ordp) or (ordp[tok1] != ordp[tok2]):
                start = NPs[i]['start']
                end = NPs[j]['end']
                pat = sent[start:end]
                pat = pat.strip()
                #print('pattern = ' + pat)
                #print('entity 1 = ' + NPs[i]['span'])
                #print('entity 2 = ' + NPs[j]['span'])
                #if (rel == ''):
                #    print('no-relation')
                #else:
                rel1 = rel2 = ''
                
                if (NPs[j]['startW'] == NPs[i]['endW'])                         and (NPs[i]['head'].lower() not in pronouns_only_s) and (NPs[j]['head'].lower() not in pronouns_only_s):
                    (rel1, span1, span2) = check_pattern_1(NPs[i], NPs[j])
                    if (rel1 != ''):
                        #relations.append(dict(entity1=span1, entity2=span2, relation=rel1, pattern=pat))
                        relations.append(rel1)

                if (NPs[j]['head'].lower() not in pronouns_only_s):
                    rel2 = check_pattern_2()

                if (rel1 == '') and (rel2 == 'be') and (NPs[j]['startW'] - NPs[i]['endW'] <= 2)                         and (NPs[i]['head'].lower() not in pronouns_only_s) and (NPs[j]['head'].lower() not in pronouns_only_s):
                    (rel1, span1, span2) = check_pattern_1(NPs[i], NPs[j])
                    tk = tokens[NPs[i]['endW']+1]['token']
                    if (rel1 != ''):
                        if (NPs[j]['startW'] - NPs[i]['endW'] < 2):
                            #relations.append(dict(entity1=span1, entity2=span2, relation=rel1, pattern=pat))
                            relations.append(rel1)
                        #elif (tk in dets):
                elif (rel2 != ''):
                    #relations.append(dict(entity1=NPs[i]['head'], entity2=NPs[j]['head'], relation=rel2, pattern=pat))
                    relations.append(rel2)
                    #print('relation = ' + rel)
            j += 1

    return relations


def get_relations(sentence):
    clearSentence()
    pre_process(sentence)
    relations = set()
    patterns = generate_Patterns()
    for rel in patterns:
        print(rel)
        relations.add(rel)

    return relations

