#! /usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask
from flask import request, jsonify, render_template
app = Flask(__name__)
from flask_cors import CORS
import os
import program
import SAO_Classify
import time
cors = CORS(app)

def get_success_result():
    return jsonify({"result": "success"})

def get_error_result(error_mess):
    return jsonify({ "result": "failure",   "error": error_mess })

@app.route('/', methods=['GET'])
def hello():
    return 'hello this is attribute extraction service :D '

@app.route('/attribute_extraction', methods=['POST', 'GET'])
def get_query_sentence():
    #start = time.time()
    try:
        sentence = request.values.get('sentence')
        relations = program.call_get_relations(sentence)
        results = {"result": "success"}
        results['answers'] = relations
        #end = time.time()
        #results['time'] = end - start
        return jsonify(results)
    except Exception as e:
        return get_error_result(str(e))

if __name__ == '__main__':
    #loaded = False
    # load the model from disk
    CURRENT_FOLDER = os.path.dirname(os.path.abspath(__file__))
    filename1 = CURRENT_FOLDER + '/Generator.pkl'
    filename2 = CURRENT_FOLDER + '/Mapping.pkl'
    filename3 = CURRENT_FOLDER + '/RandomForest.pkl'
    SAO_Classify.loadModel(filename1, filename2, filename3)
    app.run(host = '35.162.246.88', port = 7300)
