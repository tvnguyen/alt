#! /usr/bin/python
# -*- coding: utf-8 -*-

import ast
import codecs
import csv
import numpy as np
import pandas as pd
import os
from spacy.en import English

from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.svm import LinearSVC, SVC

import pickle
import time


# ## Initialization

def readList(filename):
    lst = []
    f = codecs.open(filename)
    for l in f:
        lst.append(l.strip())
    f.close()
    return lst


def readAtt2QuesDict(filename):
    dct = {}
    f = codecs.open(filename)
    reader = csv.reader(f)
    i = 0
    for att in att_types:
        dct[att] = []
    for row in reader:
        att = row[0]
        k = len(row)
        for i in range(1, k):
            dct[att].append(row[i])
    f.close()
    return dct


def init():
    global nlp, feature_generator, cols, cats, mapping,         att_types, ques_types, att2ques, ques2att, X, X1, X2, y,         pronouns_s, pronouns_only_s, pronouns_p, pronouns_o,         pronouns_map, ordp, tokens, NPs, NP_types,         loaded

    nlp = English()
    loaded = True

    feature_generator = FeatureUnion(
            transformer_list=[
                # Pipeline for pulling words features - lowercased
                ('tfidf', TfidfVectorizer(ngram_range=(1, 2), token_pattern=r'\b\w+\b', 
                                                    min_df=2, lowercase=True, norm='l2', stop_words='english')),

            ]
        )
    feature_mapping = {}
    
    cols = [u'POS_seq', u'n_tokens', u'n_stops', u'ent_prop', u'POS_left', u'POS_right', u'e1_str', u'e2_str',         u'e1_lemma', u'e2_lemma', u'verb', u'verb_extended']
    cats = [u'POS_seq', u'POS_left', u'POS_right', u'e1_str', u'e2_str', 
            u'e1_lemma', u'e2_lemma', u'verb', u'verb_extended']
    
    att_types = readList('data/attributes.csv')

    ques_types = ['what', 'where', 'when', 'who']

    att2ques = readAtt2QuesDict('data/att2ques.csv')

    ques2att = {}
    for wh in ques_types:
        ques2att[wh] = []
    for att in att2ques:
        whLst = att2ques[att]
        for i in whLst:
            wh = ques_types[i]
            ques2att[wh].append(att)

    pronouns_s = ['i', 'you', 'he', 'she', 'it', 'we', 'they']
    pronouns_only_s = ['i', 'he', 'she', 'we', 'they']
    pronouns_p = ['my', 'your', 'his', 'her', 'its', 'our', 'their']
    pronouns_o = ['mine', 'yours', 'his', 'hers', 'its', 'ours', 'theirs']
    dets = ['a', 'an', 'the']
    NP_types = ['NP', 'NNP']
    pronouns_map = {}

    ordp = {}
    ordp['i'] = ordp['my'] = ordp['mine'] = 1
    ordp['you'] = ordp['your'] = ordp['yours'] = 2
    ordp['he'] = ordp['his'] = ordp['his'] = 3
    ordp['she'] = ordp['her'] = ordp['hers'] = 4
    ordp['it'] = ordp['its'] = ordp['its'] = 5
    ordp['we'] = ordp['our'] = ordp['ours'] = 6
    ordp['they'] = ordp['their'] = ordp['theirs'] = 7

    pronouns_map['my'] = 'I'
    pronouns_map['your'] = 'You'
    pronouns_map['his'] = 'He'
    pronouns_map['her'] = 'She'
    pronouns_map['its'] = 'It'
    pronouns_map['our'] = 'We'
    pronouns_map['their'] = 'They'

    tokens = []
    NPs = []

    X = []
    X1 = []
    X2 = []
    y = []
    lst = []

# ### Cleaning

def clearAll():
    global tokens, NPs
    
    if ('tokens' in globals()):
        del tokens[:]
        del NPs[:]

def clearSentence():
    global tokens, NPs
    
    if ('tokens' in globals()):
        del tokens[:]
        del NPs[:]

def clean(sent):
    while '  ' in sent:
        sent = sent.replace('  ', ' ')
    return sent


# ### Basic functions

def countStops(startW, endW):
    n = 0
    for i in range(startW, endW):
        if (tokens[i]['is_stop']):
            n += 1
    return n


def postagSt(startW, endW):
    st = ''
    for i in range(startW, endW):
        st += ' ' + tokens[i]['pos']
    st = st.strip()
    return st


def lemmaSt(startW, endW):
    st = ''
    for i in range(startW, endW):
        st += ' ' + tokens[i]['lemma']
    st = st.strip()
    return st


# ### Noun Phrase

def overlapping1(NP1, NP2):
    startW1 = NP1['startW']
    endW1 = NP1['endW']
    startW2 = NP2['startW']
    endW2 = NP2['endW']

    if (startW1 == startW2) and (endW1 == endW2):
        return 0
    elif (startW1 <= startW2) and (endW2 <= endW1):
        return 1
    elif (startW2 <= startW1) and (endW1 <= endW2):
        return 2
    elif (startW2 <= startW1) and (startW1 <= endW2) and (endW2 <= endW1):
        return 3
    elif (startW1 <= startW2) and (startW2 <= endW1) and (endW1 <= endW2):
        return 4

    return -1


def overlapping2(NP, start, end):
    startW1 = NP['startW']
    endW1 = NP['endW']
    startW2 = start
    endW2 = end

    if (startW1 == startW2) and (endW1 == endW2):
        return 0
    elif (startW1 <= startW2) and (endW2 <= endW1):
        return 1
    elif (startW2 <= startW1) and (endW1 <= endW2):
        return 2
    elif (startW2 <= startW1) and (startW1 <= endW2) and (endW2 <= endW1):
        return 3
    elif (startW1 <= startW2) and (startW2 <= endW1) and (endW1 <= endW2):
        return 4

    return -1


def expandNP_1(startW, endW):
    i = endW-1
    while (i >= startW) and (tokens[i]['pos'] == 'NOUN'):
        i -= 1
    if (i == startW-1):
        while (i >= 0) and (tokens[i]['pos'].startswith('AD')) and (tokens[i]['token'] not in pronouns_p):
            i -= 1
        if (i >= 0) and (tokens[i]['token'] in pronouns_p):
            newStart = i
            return newStart

    return startW


def expandNP_2():
    global NPs
    n = len(tokens)
    m = len(NPs)
    i = 0
    while (i < m):
        NP = NPs[i]
        startW = NP['startW']
        endW = NP['endW']
        start = NP['start']
        end = NP['end']
        j = startW-1
        if (tokens[j]['pos'] == 'NUM'):
            while (j >= 0) and ( (tokens[j]['pos'] == 'NUM') or (tokens[j]['pos'] == 'PUNCT')                 or (tokens[j]['pos'] == 'SYM') ):
                j -= 1
            if (j >= 0):
                j += 1
            else:
                j = 0
            if (startW != j) and (tokens[j]['pos'] == 'NUM'):
                startW = j
                NP['startW'] = startW
                start = tokens[startW]['start']
                NP['start'] = start
                NP['span'] = sent[start:end]
                NP['pos'] = postagSt(startW, endW)
                NP['lemma'] = lemmaSt(startW, endW)                
                for j in reversed(range(i)):
                    if (overlapping1(NP, NPs[j]) > -1):
                        del NPs[j]
                        i -= 1
                        m -= 1
                        break

                continue

        i += 1


def insert2NPs(span, start, end, startW, endW, pos, lemma, head, NE):
    global NPs

    m = len(NPs)
    if (m == 0):
        NPs.append(dict(span=span, start=start, end=end, startW=startW, endW=endW, pos=pos, lemma=lemma, head=head, NE=NE))
    else:
        i = 0
        while (i < m) and (NPs[i]['start'] < start):
            i += 1
        if (i < m):
            NP = NPs[i]
        else:
            i -= 1
            NP = NPs[i]

        if (start == NP['start']) and (end == NP['end']):
            if (NP['NE'] in NP_types) and (NE not in NP_types):
                NP['NE'] = NE
        else:
            if (end <= NP['start']):
                NPs.insert(i, dict(span=span, start=start, end=end, startW=startW, endW=endW, pos=pos, lemma=lemma, head=head, NE=NE))
            elif (NP['end'] <= start):
                NPs.insert(i+1, dict(span=span, start=start, end=end, startW=startW, endW=endW, pos=pos, lemma=lemma, head=head, NE=NE))
            elif ((start <= NP['start']) and (NP['end'] <= end)):
                del NPs[i]
                NPs.insert(i, dict(span=span, start=start, end=end, startW=startW, endW=endW, pos=pos, lemma=lemma, head=head, NE=NE))
            elif ((start <= NP['start']) and (NP['start'] <= end) and (end <= NP['end']) ) or ((NP['start'] <= start) and (start <= NP['end']) and (NP['end'] <= end) ):
                if (NP['end']-NP['start'] < end - start):
                    del NPs[i]
                    NPs.insert(i, dict(span=span, start=start, end=end, startW=startW, endW=endW, pos=pos, lemma=lemma, head=head, NE=NE))


# ### Pre-processing
# <pre>
# Tokenization, POS tagging, lemmatization
# Noun phrases
# </pre>

def add_Wh():
    global NPs
    if (len(NPs) > 0):
        NP = NPs[0]
        tk = tokens[0]
        if (NP['start'] == tk['start']) and (NP['end'] == tk['end']):
            return
    
        span = tk['token_Ori']
        start = tk['start']
        end = tk['end']
        startW = 0
        endW = 1
        pos = tk['pos']
        lemma = tk['lemma']
        head = tk['token']
        NE = 'WH'
        insert2NPs(span, start, end, startW, endW, pos, lemma, head, NE)


def pre_process(sentence):
    global sent, tokens, NPs
    sent = clean(sentence.strip())
    if (type(sent) != unicode):
        sent = unicode(sent, 'utf-8')
    tks = nlp(sent)
    for tk in tks:
        token = tk.text.strip()
        start = tk.idx
        end = start + len(token)
        is_stop = tk.is_stop
        pos = tk.pos_
        lemma = tk.lemma_
        tokens.append(dict(token_Ori=token, token=token.lower(), start=start, end=end, is_stop=is_stop, pos=pos, lemma=lemma))

    for NP in tks.noun_chunks:
        span = NP.text.strip()
        startW = NP.start
        endW = NP.end
        startW = expandNP_1(startW, endW)
        start = tokens[startW]['start']
        end = tokens[endW-1]['end']
        span = sent[start:end]
        pos = postagSt(startW, endW)
        lemma = lemmaSt(startW, endW)
        head = NP.root.text.lower()
        head_pos = tks[NP.root.i].pos_
        NE = 'NP'
        if (head_pos == 'PROPN'):
            NE = 'NNP'
        insert2NPs(span, start, end, startW, endW, pos, lemma, head, NE)

    for NP in tks.ents:
        span = NP.text.strip()
        startW = NP.start
        endW = NP.end
        startW = expandNP_1(startW, endW)
        start = tokens[startW]['start']
        end = tokens[endW-1]['end']
        span = sent[start:end]
        pos = postagSt(startW, endW)
        lemma = lemmaSt(startW, endW)
        head = NP.root.text.lower()
        head_pos = tks[NP.root.i].pos_
        NE = NP.label_
        insert2NPs(span, start, end, startW, endW, pos, lemma, head, NE)

    expandNP_2()
    add_Wh()


# ### Find mention's positions

def locateMention(o):
    o = clean(o.strip())
    if (type(o) != unicode):
        o = unicode(o, 'utf-8')
    tks = nlp(o)
    n = len(tokens)
    m = len(tks)
    i = 0
    j = 0
    startW = endW = -1

    while (i < n) and (tokens[i]['token'] != tks[j].text.strip().lower()):
        i += 1

    while (i < n):
        startW = i
        while (i < n) and (j < m) and (tokens[i]['token'] == tks[j].text.strip().lower()):
            i += 1
            j += 1
        if (j == m):
            endW = i
        if (endW > -1):
            break
        i = startW+1
        j = 0
        while (i < n) and (tokens[i]['token'] != tks[j].text.strip().lower()):
            i += 1

    return (startW, endW)


def associateWithNP(mention, sent):
    st = sent
    (startW, endW) = locateMention(mention)
    #print(mention, startW, endW)
    start = tokens[startW]['start']
    end = tokens[endW-1]['end']

    m = len(NPs)
    for i in range(m):
        NP = NPs[i]
        ovl = overlapping2(NP, startW, endW)
        #print(ovl, mention, NP)
        if (ovl in range(0, 3)):
            #NP['span'] = st[start:end]
            #NP['start'] = start
            #NP['end'] = end
            #NP['startW'] = startW
            #NP['endW'] = endW
            #NP['pos'] = postagSt(startW, endW)
            #NP['lemma'] = lemmaSt(startW, endW)
            return i
    return -1


def locateSO(st, s, o):
    ids = ido = -1
    if (s == '?'):
        s = tokens[0]['token_Ori']
    if (s in st):
        start1 = st.index(s)
    else:
        for p in pronouns_p:
            if (s.startswith(p)):
                l = len(p)+1
                s = s[l:]
                break
    if (s in st):
        ids = associateWithNP(s, sent)
    else:
        print(st, s)

    if (o == '?'):
        o = tokens[0]['token_Ori']
    if (o in st):
        start2 = st.index(o)
    else:
        for p in pronouns_p:
            if (s.startswith(p)):
                l = len(p)+1
                o = o[l:]
                break
    if (o in st):
        ido = associateWithNP(o, sent)
    else:
        print(st, o)

    return (ids, ido)


# ## Extract Features

def check_pattern_2(NP1, NP2):
    relation = ''
    end1 = NP1['endW']
    start2 = NP2['startW']
    j = end1
    i = end1
    ok = False
    for i in range(end1, start2):
        if (not tokens[i]['pos'] == 'VERB') and (tokens[i]['pos'] != 'ADV') and (tokens[i]['pos'] != 'ADP'):
            break
        elif (tokens[i]['pos'] == 'VERB'):
            ok = True
            j = i
    if (ok) and (i == start2-1):
        relation = lemmaSt(j, j+1)
        relationExt = lemmaSt(j, j+2)
        relation = relation.strip()
        relationExt = relationExt.strip()
        return (relation, relationExt)
    return ('', '')


def generate_DisFeatures(NP1, NP2):
    start1 = NP1['startW']
    end1 = NP1['endW']
    start2 = NP2['startW']
    end2 = NP2['endW']

    # F1: The POS tag sequence in r_ij
    pos_seq = postagSt(end1, start2)

    # F2: the number of tokens un r_ij
    n_tks = start2 - end1 - 1

    # F3: the number of stopwords in r_ij
    n_stops = countStops(end1, start2)

    #F4: whether or not an object e is found to be a proper noun
    ONP = 0
    if (NP1['NE'] == NP2['NE']):
        if (NP1['NE'] == 'NNP'):
            ONP = 1
        else:
            ONP = 2
    else:
        if (NP1['NE'] == 'NNP'):
            ONP = 3
        else:
            ONP = 4
    
    # F5: the POS tag to the left of e1
    pos1 = ''
    if (start1 > 0):
        pos1 = tokens[start1-1]['pos']
    
    # F6: the POS tag to the right of e2
    pos2 = ''
    if (end2 < len(tokens)):
        pos2 = tokens[end2]['pos']
    
    # F7: the lexical string of e1
    pat1 = NP1['span']
    
    # F8: the lexical string of e2
    pat2 = NP2['span']
    
    # F9: the lexical string of e1
    patLemma1 = NP1['lemma']
    
    # F10: the lexical string of e2
    patLemma2 = NP2['lemma']
    
    #F11: the main verb (lemmatised) appearing between the two entity
    #F12: the main verb and the word to the its right appearing between the two entity
    # live-live in, settle, settle at
    (verb, verbExt) = check_pattern_2(NP1, NP2)
    
    inst = [pos_seq, n_tks, n_stops, ONP, pos1, pos2, pat1, pat2, patLemma1, patLemma2, verb, verbExt]
    return inst


def transform_Features(mode='train'):
    global X1, X2, mapping
    df = pd.DataFrame(columns = cols)
    n = len(X1)
    for i in range(n):
        df.loc[i] = X1[i]

    if (mode == 'train'):
        mapping = {}
        feature_mapping = {}
        for category in cats:
            feature_mapping = {}
            #df.ix[:, category] = df.ix[:, category].astype('category').cat.codes.values
            col = df.ix[:, category].tolist()
            col = list(np.unique(col))
            if ('' in col):
                col.remove('')
            n = len(col)

            for i in range(n):
                feature_mapping[col[i]] = i+1

            df.ix[:, category] = df.ix[:, category].apply(lambda k: feature_mapping[k] if k in feature_mapping else 0)
            mapping[category] = feature_mapping
            X_feats = feature_generator.fit_transform(X2)
    

    elif (mode == 'test'):
        feature_mapping = {}
        for category in cats:
            feature_mapping = mapping[category]
            df.ix[:, category] = df.ix[:, category].apply(lambda k: feature_mapping[k] if k in feature_mapping else 0)
            X_feats = feature_generator.transform(X2)
    
    X1 = df.values
    X2 = X_feats.toarray()

    return mapping


# ## Generate potential triples between pair of mentions

def generateFeatures(idx=-1, a = '', direction=1, mode='train'):
    #sentence = dct['sent']
    #pre_process(sentence)
    n = len(tokens)
    m = len(NPs)
    WH = NPs[0]
    X1_sent = []
    X2_sent = []
    y_sent = []
    for i in range(1, m):
        # discrete features
        inst = generate_DisFeatures(WH, NPs[i])
        X1_sent.append(inst)

        end = NPs[i]['end']
        pattern = sent[:end]
        X2_sent.append(pattern)

        if (mode == 'train'):
            if (i == idx):
                ida = att_types.index(a) + 1
                if (direction == 1):
                    y_sent.append(ida)
                else:
                    y_sent.append(ida + len(att_types))
            else:
                y_sent.append(0)

    return (X1_sent, X2_sent, y_sent)


def readDataFile(filename):
    global X, X1, X2, y, lst, mapping
    X = []
    X1 = []
    X2 = []
    y = []
    f = codecs.open(filename)
    reader = csv.reader(f)
    lst = []
    i = 0
    for row in reader:
        # Origin sentence
        st = row[2]
        dct = ast.literal_eval(st)
        dct['sent'] = row[1]
        direction = 1

        sentence = dct['sent']
        clearSentence()
        pre_process(sentence)
        (ids, ido) = locateSO(sentence, dct['S'], dct['O'])
        
        if (ids > -1) and (ido > -1) and ((ids == 0) or (ido == 0) ):
            i += 1
            if (ids == 0):
                dct['idx'] = ido
                dct['direction'] = 1
            elif (ido == 0):
                dct['idx'] = ids
                dct['direction'] = 0
            lst.append(dct)
            (X1_sent, X2_sent, y_sent) = generateFeatures(dct['idx'], dct['A'], dct['direction'])
            
            X1 += X1_sent
            X2 += X2_sent
            y += y_sent
        #else:
            #print(dct)
            #print(NPs)
            #print(ids, ido)
            #print

        # Question without ?
        st = row[2]
        dct2 = ast.literal_eval(st)

        st = row[1]
        for q in ques_types:
            if (st.lower().startswith(q)) and (st.endswith('?')):
                l = len(st)
                st = st[:(l-1)].strip()
                break

        dct2['sent'] = st
        direction = 1

        sentence = dct2['sent']
        clearSentence()
        pre_process(sentence)
        (ids, ido) = locateSO(sentence, dct2['S'], dct2['O'])
        
        if (ids > -1) and (ido > -1) and ((ids == 0) or (ido == 0) ):
            i += 1
            if (ids == 0):
                dct2['idx'] = ido
                dct2['direction'] = 1
            elif (ido == 0):
                dct2['idx'] = ids
                dct2['direction'] = 0
            lst.append(dct2)
            (X1_sent, X2_sent, y_sent) = generateFeatures(dct2['idx'], dct2['A'], dct2['direction'])
            
            X1 += X1_sent
            X2 += X2_sent
            y += y_sent

    mapping = transform_Features()
    f.close()

    print(i)
    return lst

# ### Cross Validation

def cross_validation(X, y, n_folds = 5):
    n = X.shape[0]
    subset_size = n/n_folds
    clf = RandomForestClassifier(n_estimators=150)
    F1 = 0
    for i in range(n_folds):
        X_te = X[i*subset_size:][:subset_size]
        X_tr = np.vstack((X[:i*subset_size], X[(i+1)*subset_size:]))
        y_te = y[i*subset_size:][:subset_size]
        y_tr = y[:i*subset_size] + y[(i+1)*subset_size:]
        clf.fit(X_tr, y_tr)
        F1 += clf.score(X_te, y_te)
    F1 = (F1/n_folds)
    return F1


def readDataAndValidation(filename):
    global X, clf
    if ('loaded' not in globals()):
        init()
    lst = readDataFile(filename)

    X = np.hstack((X1, X2))
    print('Data size: ' + str(X.shape[0]) + ', ' + str(X.shape[1]))
    print('Positive examples: ' + str(len(y) - y.count(0)))
    print('Negative examples: ' + str(y.count(0)))
    F1 = cross_validation(X, y)
    print('Validation score: ' + str(F1))


# ### Read Data and Train

def training(X, y):
    clf = RandomForestClassifier(n_estimators=50)
    clf.fit(X, y)
    return clf


def readDataAndTrain(filename):
    global X, clf
    if ('loaded' not in globals()):
        init()

    lst = readDataFile(filename)
    X = np.hstack((X1, X2))
    clf = training(X, y)

    # save the model to disk
    filename = 'Generator.pkl'
    pickle.dump(feature_generator, open(filename, 'wb'))

    # save the model to disk
    filename = 'Mapping.pkl'
    pickle.dump(mapping, open(filename, 'wb'))

    # save the model to disk
    filename = 'RandomForest.pkl'
    pickle.dump(clf, open(filename, 'wb'))

if __name__ == '__main__':
    filename = 'data/question_all_set.csv'
    #filename = 'data/question_training_set.csv'
    readDataAndTrain(filename)
